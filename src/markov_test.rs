use rand::{self, prelude::*, Rng};
use std::collections::HashMap;
use serde_json;

type TGraphIn = Vec<(Vec<String>, Vec<(i32, String)>)>;
type TGraph = HashMap<Vec<String>, Vec<(i32, String)>>;

pub struct Mark {
    graph : TGraph,
}

impl Mark {
    pub fn new(input : &str) -> Result<Self, serde_json::Error> {
        let graph : TGraphIn = serde_json::from_str(input)?;
        let graph = Self::to_graph(graph);
        Ok(Self {
            graph
        })
    }

    pub fn gen(&self, word_count : i32) -> String {
        let mut prev_words : Vec<String> = Vec::new();
        let mut res = String::new();
        let mut space_before = "";
        for _i in 0..word_count {
            let entry = self.graph.get(&prev_words);
            let entry = match entry {
                Some(e) => e,
                None => { if prev_words.len() == 0 { break; } else { prev_words.remove(0); continue; } },
            };
            let search_key = {
                let sum : i32 = entry.iter().map(|(x,_)| x).sum();
                let rand_val : i32 = thread_rng().gen_range(0, sum);
                // if _i == 0 { println!("{}", rand_val); }
                // rand_val % sum
                rand_val
            }; 
            let found_val = Self::choose(search_key, &entry);
            let found_val = match found_val {
                Some(f) => f,
                None => { prev_words.remove(0); continue; }
            };

            res += &format!("{}{}", space_before, found_val);
            space_before = " ";
            
            prev_words.push(found_val.clone());

            if found_val == "" {
                break;
            }
        }
        res
    }

    fn choose(search_key : i32, entry : &Vec<(i32, String)>) -> Option<&String> {
        let mut start_val = 0;
        for (e, r) in entry {
            if start_val + e > search_key {
                return Some(r);
            }
            start_val += e;
        }
        if entry.len() == 0 { return None; }
        return Some(&entry[entry.len() - 1].1);
    }

    fn to_graph(graph_in : TGraphIn) -> TGraph {
        let mut res = TGraph::new();
        for (k,v) in graph_in {
            res.insert(k, v); 
        }
        res
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic() {
        let foo = r#"
            [
                [[], [[1, "foo"]]],
                [["foo"], [[2, "bar"]]],
                [["foo", "bar"], [[3, "baz"]]]
            ]
        "#;
        let test_mark = Mark::new(&foo).unwrap();
        assert_eq!(test_mark.gen(3), "foo bar baz");
    }

    #[test]
    fn prob() {
        let foo = r#"
            [
                [[], [[1, "foo"]]],
                [["foo"], [[2, "bar"]]],
                [["foo", "bar"], [[3, "baz"], [3, "quix"]]]
            ]
        "#;
        let test_mark = Mark::new(&foo).unwrap();
        let mut baz_found = false;
        let mut quix_found = false;
        for _ in 0..10 {
            let res = test_mark.gen(3);
            if res == "foo bar baz" { baz_found = true; }
            else if res == "foo bar quix" { quix_found = true; }
        }
        assert!(baz_found && quix_found);
    }
}
