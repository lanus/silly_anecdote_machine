extern crate rand;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate rouille;

mod markov_test;

use rouille::{Request, Response};
use std::fs::File;

fn main() {
    let instr = include_str!("../data/out.txt");
    let mark = markov_test::Mark::new(instr).unwrap();

    // rouille::start_server("178.62.236.221:8081", move |request| {
    rouille::start_server("0.0.0.0:8596", move |request| {
        router!(request,
            (GET) (/) => {
                Response::html(include_str!("../data/index.html"))
            },
            (GET) (/anec) => {
                Response::text(mark.gen(1000).replace("\n", "<br>"))
            },
            _ => Response::empty_404()
        )
    });
}
