Silly Anecdote Machine

Random anecdote generator

INSTALLATION:

1. (optional) Compile anecdotes:
```
cd data && mark.py <input_file> <output_file>
```
, where <input_file> - anecdotes, one by line (original is lost btw)
	  <output_file> - huge json file that will be used for random generation

Or use `out.txt`

2. Edit `src/main.rs`:
	"0.0.0.0:8596" - to address:port
	"../data/out.txt" - to json file that mark.py has generated

3. Install Rust and run server:
```
# Download rustup, and follow on-screen instructions, install last stable version
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
# Compile server
cargo build --release
cp target/release/silly_anecdote_machine .

```

4. Run server
```
./silly_anecdote_machine
```
