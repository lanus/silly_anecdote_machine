#!/usr/bin/env python3

import sys
import json
import re

re_start = re.compile(r"^\d*\.?\s+(\S.*)")

def analyze(files):
    prob = dict()
    def addprob(pw, w):
        if pw == () and w == "":
            return

        # print(pw, "\"%s\""%w)
        if pw not in prob:
            prob[pw] = dict()
        if w not in prob[pw]:
            prob[pw][w] = 1
        else:
            prob[pw][w] += 1

    for fin in files:
        prev_words = tuple() 
        for fline in open(fin,"r"):
            if fline == "":
                prev_words = ()

            if fline == "\n":
                addprob(prev_words, "")
                prev_words = ()
                continue

            matches = re_start.search(fline) 
            was_tab = False
            if matches is not None:
                was_tab = True
                fline = matches.groups()[0]

            splitted = fline.split()
            if was_tab == True and len(prev_words) > 0:
                splitted = ["\n"] + splitted

            for fword in splitted:
                addprob(prev_words, fword)

                if len(prev_words) == 0:
                    prev_words = (fword, )
                else:
                    prev_words = tuple(list(prev_words) + [fword])
                    if len(prev_words) > 3:
                        prev_words = tuple(list(prev_words)[-3:])
 
    res = []
    for p in prob:
        resval = []
        for v in prob[p]:
            resval.append([prob[p][v], v])
        res.append([p, resval])

    return res

if __name__ == "__main__":
    fout = sys.argv[1]
    fins = sys.argv[2:]
    res = analyze(fins)
    open(fout, 'w').write(json.dumps(res, indent=4, ensure_ascii=False))
    # print(json.dumps(res, sort_keys=True, indent=4))

